FROM openjdk:latest
COPY target/Hangman-jar-with-dependencies.jar Hangman-jar-with-dependencies.jar
CMD java -jar Hangman-jar-with-dependencies.jar
